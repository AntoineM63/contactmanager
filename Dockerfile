FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY src/ContactManager/*.csproj ./src/ContactManager/
COPY tests/ContactManager.UnitTests/*.csproj ./tests/ContactManager.UnitTests/
COPY tests/ContactManager.FunctionnalTests/*.csproj ./tests/ContactManager.FunctionnalTests/
RUN dotnet restore

# copy everything else and build app
COPY src/ContactManager/. ./src/ContactManager/
WORKDIR /app/src/ContactManager
RUN dotnet publish -c Release -o out

# Run Unit Tests
FROM build AS unittest
WORKDIR /app/tests/ContactManager.UnitTests
COPY tests/ContactManager.UnitTests/. .
ENTRYPOINT ["dotnet", "test"]

# Run Fonctionnal Tests
FROM build AS functionnaltest
WORKDIR /app/tests/ContactManager.FunctionnalTests
COPY tests/ContactManager.FunctionnalTests/. .
ENTRYPOINT ["dotnet", "test"]

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/src/ContactManager/out ./
ENTRYPOINT ["dotnet", "ContactManager.dll"]

